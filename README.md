### Dependencies
Ionic
[Angular formly](https://github.com/formly-js/angular-formly)
[ngMask](https://github.com/candreoliveira/ngMask)
[Angular formly ionic](https://github.com/formly-js/angular-formly-templates-ionic)
[intlpnIonic](https://github.com/pierre-vigier/intlpnIonic)
[upload-photo-component](https://bitbucket.org/findibr/upload-photo-component.git)
[fix-mask-input](https://github.com/ivanguimam/fix-mask-input.git)
```html
<link rel="stylesheet" href="lib/intlpnIonic/css/intlpn.css">

<script src="lib/ionic/js/ionic.bundle.js"></script>
<script src="lib/api-check/dist/api-check.min.js"></script>
<script src="lib/angular-formly/dist/formly.min.js"></script>
<script src="lib/angular-formly-templates-ionic/dist/angular-formly-templates-ionic.js"></script>
<script src="lib/user-form/user-form.js"></script>
<script src="lib/intlpnIonic/js/intlpnIonic.js"></script>
<script src="lib/intlpnIonic/js/data.js"></script>
<script src="lib/intlpnIonic/lib/libphonenumber/build/utils.js"></script>
<script src='lib/ngMask/dist/ngMask.min.js'></script>
<script src="lib/fix-mask-input/fix-mask-input.js"></script>
<script src="lib/upload-photo-component/upload-photo-component.js"></script>
```

### Insert module
```javascript
angular.module('myModule', ['findi.userForm'])
```

### Use
```html
<user-form ng-model="myModel" options="inputOptions"></user-form>
```
### inputOptions
```javascript
{
	configFields: {
		name: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		password: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model,
			minlength: 'INT' // Change minlength input
		},
		email: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		cpf: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		phone: { // optional
			searchPlaceholder: 'STRING'// Modal search field placeholder
			defaultContry: 'STRING' // Set default country
			boxHeaderTitle: 'STRING' // Title to header search modal
			countryList: 'ARRAY_OF_STRING' // Array with available countries for selection(If not set, all countries will be available for selection)
		},
		birthdday: {
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model,
			minlength: 'INT' // Change minlength input.
			required: 'BOOLEAN'
		},
		gender: {
			hide: 'BOOLEAN', // Remove input
			label: 'STRING', // label
			label1: 'STRING', // Female label
			label2: 'STRING', // Male label
			value1: 'STRING', // Female value
			value2: 'STRING' // Male value
		},
		photo: {
			hide: 'BOOLEAN',
			key: 'STRING',
			required: 'BOOLEAN',
			templateUrl: 'STRING',
			titleActionSheet: 'STRING',
			actionSheetCameraText: 'STRING',
			actionSheetGaleryText: 'STRING',
			actionSheetCancelText: 'STRING',
			uploadUrl: 'STRING'
		}
	},
	submit: function(data, form) { // required
		// data: myModel
		// form: form
	},
	submitButtonText: 'Vai' // Text button submit
}
```
