(function() {
	'use strict'

	angular.module('findi.userForm', ['ionic', 'formlyIonic', 'intlpnIonic',
			'ngMask', 'fixMaskInput', 'ionicUpload'
		])
		.component('userForm', {
			template: '<form name="{{$ctrl.opts.formName}}" novalidate="novalidate" ng-submit="$ctrl.opts.submit($ctrl.ngModel)">' +
				'<formly-form model="$ctrl.ngModel" fields="$ctrl.opts.fields"></formly-form>' +
				'<input class="findi-btn" type="submit" value="{{$ctrl.opts.submitButtonText}}"/>' +
				'</form>',
			controller: userFormController,
			controllerAs: '$ctrl',
			bindings: {
				ngModel: '=',
				options: '='
			}
		})

	function userFormController($scope, formlyConfig) {
		var vm = this
		var optionsParams = angular.copy(vm.options) || {}
		vm.opts = _buildOpts(vm, optionsParams)

		_createFormlyTypes(formlyConfig, optionsParams)
	}

	var _buildOpts = function(vm, optionsParams) {
		var opts = {}
		opts.formName = "$ctrl.registerUserForm"
		opts.configFields = optionsParams.configFields || {}
		opts.submitButtonText = optionsParams.submitButtonText || 'Continuar'
		opts.fields = _getFields(opts.configFields)
		opts.submit = function(data) {
			optionsParams.submit(data, vm.registerUserForm)
		}

		return opts
	}

	var _createFormlyTypes = function(formlyConfig, optionsParams) {
		var formlyTypes = formlyConfig.getTypes()

		if (!formlyTypes['input-gender']) {
			formlyConfig.setType({
				name: 'input-gender',
				template: '<div class="item item-input item-stacked-label item-input-gender">' +
					'<div class="wrapper">' +
					'<span class="input-label">{{options.templateOptions.label}}</span>' +
					'<div class="box-label-gender">' +
					'<label class="control control--radio">' +
					'<input type="radio" ng-model="model[options.key]" id="gender-1" ng-value="options.templateOptions.value1"/>' +
					'<div class="control__indicator"></div>' +
					'</label>' +
					'<label for="gender-1" class="label-gender">{{options.templateOptions.label1}}</label>' +
					'</div>' +
					'<div class="box-label-gender">' +
					'<label class="control control--radio">' +
					'<input type="radio" ng-model="model[options.key]" id="gender-2" ng-value="options.templateOptions.value2"/>' +
					'<div class="control__indicator"></div>' +
					'</label>' +
					'<label for="gender-2" class="label-gender">{{options.templateOptions.label2}}</label>' +
					'</div>' +
					'</div>' +
					'</div>'
			})
		}

		if (!formlyTypes.photo) {
			var configFields = optionsParams.configFields || {}
			var configFieldPhoto = configFields.photo || {}

			formlyConfig.setType({
				name: 'photo',
				template: '<ionic-upload ng-model="model[options.key]"' +
					'options="options.templateOptions.config" template-url="' +
					configFieldPhoto.templateUrl +
					'" formly-custom-validation="options.validators"></ionic-upload>'
			})
		}
	}

	var _getFields = function(configFields) {
		var array = []
		var configFieldPhoto = configFields.photo || {}
		var configFieldName = configFields.name || {}
		var configFieldEmail = configFields.email || {}
		var configFieldPassword = configFields.password || {}
		var configFieldBirthday = configFields.birthday || {}
		var configFieldGender = configFields.gender || {}
		var configFieldCpf = configFields.cpf || {}
		var configFieldPhone = configFields.phone || {}

		if (!configFieldPhoto.hide) array.push(_getPhotoField(configFieldPhoto))
		if (!configFieldName.hide) array.push(_getNameField(configFieldName))
		if (!configFieldEmail.hide) array.push(_getEmailField(configFieldEmail))
		if (!configFieldPassword.hide) array.push(_getPassField(configFieldPassword))
		if (!configFieldBirthday.hide) array.push(_getBirthdayField(
			configFieldBirthday))
		if (!configFieldGender.hide) array.push(_getGenderField(configFieldGender))
		if (!configFieldCpf.hide) array.push(_getCpfField(configFieldCpf))
		if (!configFieldPhone.hide) array.push(_getPhoneField(configFieldPhone))

		return array
	}

	var _getPhotoField = function(configField) {
		return {
			key: configField.key ? configField.key : "photo_url",
			type: "photo",
			templateOptions: {
				config: configField ? configField : {}
			}
		}
	}

	var _getNameField = function(configField) {
		return {
			key: configField.key ? configField.key : "name",
			type: "stacked-input",
			templateOptions: {
				required: configField.required === false ? false : true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'NOME',
				disabled: configField.disabled ? configField.disabled : false
			},
			validators: {
				compound_name: {
					expression: function($viewValue, $modelValue) {
						return $viewValue && $viewValue.split(' ').length >= 2
					}
				}
			}
		}
	}

	var _getEmailField = function(configField) {
		return {
			key: configField.key ? configField.key : "email",
			type: "stacked-input",
			templateOptions: {
				required: configField.required === false ? false : true,
				type: "email",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'E-MAIL',
				disabled: configField.disabled ? configField.disabled : false
			}
		}
	}

	var _getPassField = function(configField) {
		return {
			key: configField.key ? configField.key : "password",
			type: "stacked-input",
			templateOptions: {
				required: configField.required === false ? false : true,
				type: "password",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'SENHA',
				minlength: configField.minlength ? configField.minlength : 8,
				disabled: configField.disabled ? configField.disabled : false
			}
		}
	}

	var _getBirthdayField = function(configField) {
		return {
			key: configField.key ? configField.key : "birthday",
			type: "stacked-input",
			id: 'input-bithday',
			ngModelAttrs: {
				mask: {
					attribute: 'mask'
				},
				maskClean: {
					attribute: 'mask-clean'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			},
			templateOptions: {
				required: configField.required === false ? false : true,
				type: "tel",
				mask: '99/99/9999',
				maskClean: true,
				maskInput: '',
				label: configField.label ? configField.label : 'DATA DE NASCIMENTO',
				disabled: configField.disabled ? configField.disabled : false
			}
		}
	}

	var _getGenderField = function(configField) {
		return {
			key: configField.key ? configField.key : 'gender',
			type: 'input-gender',
			templateOptions: {
				label: configField.label ? configField.label : 'SEXO',
				label1: configField.label1 ? configField.label1 : 'Feminino',
				label2: configField.label2 ? configField.label2 : 'Masculino',
				value1: configField.value1 ? configField.value1 : 'female',
				value2: configField.value2 ? configField.value2 : 'male'
			}
		}
	}

	var _getCpfField = function(configField) {
		return {
			key: configField.key ? configField.key : "cpf",
			type: "stacked-input",
			id: 'input-cpf',
			ngModelAttrs: {
				mask: {
					attribute: 'mask'
				},
				maskClean: {
					attribute: 'mask-clean'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			},
			templateOptions: {
				required: configField.required === false ? false : true,
				type: "tel",
				mask: '999.999.999-99',
				maskClean: true,
				maskInput: '',
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'CPF',
				disabled: configField.disabled ? configField.disabled : false
			},
			validators: {
				cpf: {
					expression: function($viewValue, $modelValue) {
						var _verifyCpf = function(cpf, digitToCheck) {
							var array = cpf.split('')
							var sum = 0
							var j = digitToCheck == 'FIRST_DIGIT' ? 10 : 11
							var length = digitToCheck == 'FIRST_DIGIT' ? 9 : 10
							var comparatorDigit = digitToCheck == 'FIRST_DIGIT' ? 9 : 10

							var isEquals = true
							for (var i = 1; i <= array.length - 1; i++) {
								if (array[i] != array[i - 1]) {
									isEquals = false
								}
							}

							if (isEquals) return false

							for (var i = 0; i < length; i++) {
								sum = sum + (parseInt(array[i]) * j)
								j--
							}

							var verifyingDigit = (sum % 11) < 2 ? 0 : 11 - (sum % 11)

							if (verifyingDigit != array[comparatorDigit]) {
								return false
							}

							return true
						}

						return /^[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}$/.test($viewValue) &&
							_verifyCpf($modelValue, 'FIRST_DIGIT') && _verifyCpf($modelValue,
								'SECOND_DIGIT')
					},
				}
			}
		}
	}

	var _getPhoneField = function(configField) {
		return {
			key: configField.key ? configField.key : "phone",
			type: "stacked-input",
			id: 'input-cpf',
			ngModelAttrs: {
				mask: {
					attribute: 'mask'
				},
				maskClean: {
					attribute: 'mask-clean'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			},
			templateOptions: {
				required: configField.required === false ? false : true,
				type: "tel",
				mask: '+55 (99) 9?9999-9999',
				maskClean: true,
				maskInput: '',
				placeholder: configField.placeholder ? configField.placeholder : "+55 (XX) XXXXX-XXXX",
				label: configField.label ? configField.label : 'TELEFONE',
				disabled: configField.disabled ? configField.disabled : false
			}
		}
	}
})()
